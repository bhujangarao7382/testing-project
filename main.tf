provider "aws" {
  region                  = "ap-south-1"
  profile                 = "myaws"
}

resource "aws_vpc" "development-vpc" {
  cidr_block = "192.168.0.0/16"
}

resource "aws_subnet" "india-subnet-1" {
  vpc_id     = aws_vpc.development-vpc.id
  cidr_block = "192.168.160.0/24"
  availability_zone = "ap-south-1a"
}